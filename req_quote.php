<?php // validating if user logged in or not


require_once("auth.php");
// validating if user logged in or not
require_once("connection.php");

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Quote Request | Clipping Path Universe </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for buttons extension demos" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"
    />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css"
    />
    <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"
    />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include('header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse in" aria-expanded="true">
                    <!-- END SIDEBAR MENU -->
                    <div class="page-sidebar-wrapper">
                        <!-- BEGIN RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                        <ul class="page-sidebar-menu visible-sm visible-xs  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true"
                            data-slide-speed="200">

                            <li class="nav-item start active">
                                <a href="#" class="nav-link nav-toggle"> Dashboard
                                    <span class="selected"> </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle"> Orders
                                    <span class="arrow"> </span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="javascript:;" class="nav-link nav-toggle"> My Orders</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:;" class="nav-link nav-toggle"> Place an order</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:;" class="nav-link nav-toggle"> Request a quote</a>
                                    </li>

                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link nav-toggle"> Download
                                    <span class="selected"> </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link nav-toggle"> Invoice
                                    <span class="selected"> </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link nav-toggle"> Account
                                    <span class="selected"> </span>
                                </a>
                            </li>

                        </ul>
                        <!-- END RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                    </div>
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->


                    <!-- BEGIN PAGE TITLE-->
                    <h1 class="page-title">
                        <i class="fa fa-plus-circle"></i> Request a quotation </h1>
                    <hr>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <!-- Begin: life time stats -->
                            <d
                            
                            iv class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list"></i>
                                        <span class="caption-subject sbold uppercase">All Orders</span>
                                    </div>
                                    <div class="actions">
                                        <button type="button" class="btn btn-circle btn-sm green-meadow">
                                            <i class="fa fa-plus"></i> Create a new order</button>
                                        <div class="btn-group">
                                            <a class="btn red btn-circle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i>
                                                <span class="hidden-xs"> Trigger Tools </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                                <li>
                                                    <a href="javascript:;" data-action="0" class="tool-action">
                                                        <i class="icon-printer"></i> Print</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="1" class="tool-action">
                                                        <i class="icon-check"></i> Copy</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="2" class="tool-action">
                                                        <i class="icon-doc"></i> PDF</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="3" class="tool-action">
                                                        <i class="icon-paper-clip"></i> Excel</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="4" class="tool-action">
                                                        <i class="icon-cloud-upload"></i> CSV</a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;" data-action="5" class="tool-action">
                                                        <i class="icon-refresh"></i> Reload</a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover" id="sample_3">
                                            <thead>
                                                <tr>
                                                    <th> Rendering engine </th>
                                                    <th> Browser </th>
                                                    <th> Platform(s) </th>
                                                    <th> Engine version </th>
                                                    <th> CSS grade </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td> Trident </td>
                                                    <td> Internet Explorer 4.0 </td>
                                                    <td> Win 95+ </td>
                                                    <td> 4 </td>
                                                    <td> X </td>
                                                </tr>
                                                <tr>
                                                    <td> Trident </td>
                                                    <td> Internet Explorer 5.0 </td>
                                                    <td> Win 95+ </td>
                                                    <td> 5 </td>
                                                    <td> C </td>
                                                </tr>
                                                <tr>
                                                    <td> Trident </td>
                                                    <td> Internet Explorer 5.5 </td>
                                                    <td> Win 95+ </td>
                                                    <td> 5.5 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Trident </td>
                                                    <td> Internet Explorer 6 </td>
                                                    <td> Win 98+ </td>
                                                    <td> 6 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Trident </td>
                                                    <td> Internet Explorer 7 </td>
                                                    <td> Win XP SP2+ </td>
                                                    <td> 7 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Trident </td>
                                                    <td> AOL browser (AOL desktop) </td>
                                                    <td> Win XP </td>
                                                    <td> 6 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Firefox 1.0 </td>
                                                    <td> Win 98+ / OSX.2+ </td>
                                                    <td> 1.7 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Firefox 1.5 </td>
                                                    <td> Win 98+ / OSX.2+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Firefox 2.0 </td>
                                                    <td> Win 98+ / OSX.2+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Firefox 3.0 </td>
                                                    <td> Win 2k+ / OSX.3+ </td>
                                                    <td> 1.9 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Camino 1.0 </td>
                                                    <td> OSX.2+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Camino 1.5 </td>
                                                    <td> OSX.3+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Netscape 7.2 </td>
                                                    <td> Win 95+ / Mac OS 8.6-9.2 </td>
                                                    <td> 1.7 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Netscape Browser 8 </td>
                                                    <td> Win 98SE+ </td>
                                                    <td> 1.7 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Netscape Navigator 9 </td>
                                                    <td> Win 98+ / OSX.2+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.0 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.1 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1.1 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.2 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1.2 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.3 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1.3 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.4 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1.4 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.5 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1.5 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.6 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> 1.6 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.7 </td>
                                                    <td> Win 98+ / OSX.1+ </td>
                                                    <td> 1.7 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Mozilla 1.8 </td>
                                                    <td> Win 98+ / OSX.1+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Seamonkey 1.1 </td>
                                                    <td> Win 98+ / OSX.2+ </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Gecko </td>
                                                    <td> Epiphany 2.20 </td>
                                                    <td> Gnome </td>
                                                    <td> 1.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> Safari 1.2 </td>
                                                    <td> OSX.3 </td>
                                                    <td> 125.5 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> Safari 1.3 </td>
                                                    <td> OSX.3 </td>
                                                    <td> 312.8 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> Safari 2.0 </td>
                                                    <td> OSX.4+ </td>
                                                    <td> 419.3 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> Safari 3.0 </td>
                                                    <td> OSX.4+ </td>
                                                    <td> 522.1 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> OmniWeb 5.5 </td>
                                                    <td> OSX.4+ </td>
                                                    <td> 420 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> iPod Touch / iPhone </td>
                                                    <td> iPod </td>
                                                    <td> 420.1 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Webkit </td>
                                                    <td> S60 </td>
                                                    <td> S60 </td>
                                                    <td> 413 </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 7.0 </td>
                                                    <td> Win 95+ / OSX.1+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 7.5 </td>
                                                    <td> Win 95+ / OSX.2+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 8.0 </td>
                                                    <td> Win 95+ / OSX.2+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 8.5 </td>
                                                    <td> Win 95+ / OSX.2+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 9.0 </td>
                                                    <td> Win 95+ / OSX.3+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 9.2 </td>
                                                    <td> Win 88+ / OSX.3+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera 9.5 </td>
                                                    <td> Win 88+ / OSX.3+ </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Opera for Wii </td>
                                                    <td> Wii </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Nokia N800 </td>
                                                    <td> N800 </td>
                                                    <td> - </td>
                                                    <td> A </td>
                                                </tr>
                                                <tr>
                                                    <td> Presto </td>
                                                    <td> Nintendo DS browser </td>
                                                    <td> Nintendo DS </td>
                                                    <td> 8.5 </td>
                                                    <td> C/A
                                                        <sup>1</sup>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>


                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php include('footer.php'); ?>
            <!-- END FOOTER -->
        </div>

        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>