<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
	<!-- BEGIN Authentication and DB Connection kulsum01s -->
	<?php // validating if user logged in or not
	require_once("auth.php");
	// validating if user logged in or not
	require_once("connection.php");

     if (isset($_GET["id"])){ 
        $receivedValue = $_GET["id"];   
    } else {
        $receivedValue = "";
    
    }
    
    $user_id = $receivedValue;

      $sql = "SELECT * From instructor_list WHERE id = '$user_id'";
      $result = $conn->query($sql);
   
            
     while($row = $result->fetch_assoc()) {

      $f_name = $row["f_name"];
      $email = $row["email"];
		$address = $row["address"];
		$city = $row["city"];
		$country = $row["country"];
		$login_id =$row["login_id"];
		$password =$row["password"];
      $user_type =$row["type"];

     }
	?>
	<!-- end Authentication and DB Connection kulsum01e -->
    <head>
        <meta charset="utf-8" />
        <title>IPSTQM | Edit User</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS kulsum02start-->
         <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS kulsum02end-->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "admin_header.php";?>
			<!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <?php include "sidebar_menu_admin.php";?>
				<!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                            
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Edit System User
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                   
                    
                    
                        
                    <!--notification end-->
						<!-- BEGIN Kaizen page Content kulsum03start-->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-user font-red-sunglo"></i>
                                                        <span class="caption-subject font-red-sunglo bold uppercase">System User</span>
                                                        <span class="caption-helper">Edit / Delete user from this system</span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="user_profile_insert.php" method="POST" class="form-horizontal">
                                                        <div class="form-body">

                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Full Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="fullname" value="<?php echo $f_name;?>"  placeholder="Enter text">
                                                                </div>
                                                            </div>

                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Email Address</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" class="form-control" name="email"  value="<?php echo $email;?>" placeholder="Email Address"> </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Address</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="address" value="<?php echo $address;?>"  placeholder="Enter text">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">City/Town</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="city" value="<?php echo $city;?>"  placeholder="Enter text">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Country</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="country" value="<?php echo $country;?>"  placeholder="Enter text">
                                                                </div>
                                                            </div>
                                                         
                                                           
                                                          
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">User Type</label>
                                                                    <div class="col-md-4">
                                                                        <select class="form-control" name="type">

                                                                        <option value="">Select User Type</option>

                                                                        <option value="Admin" <?php 
                                                                                     
                                                                         if ($user_type == "Admin"){
                                                                              echo "selected='selected' ";
                                                                                }
                                                                        ?> >Admin</option>

                                                                        <option value="User" <?php 
                                                                                     
                                                                         if ($user_type == "User"){
                                                                              echo "selected='selected' ";
                                                                                }
                                                                        ?> >User</option>
                                                                      
                                                                        </select>
                                                                 </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                           <a data-toggle="modal" href="#basic" class="btn btn-danger">Delete</a>

                                                             <a href="view_user_list.php" class="btn btn-default">Go Back</a>  
                                                             <input type="hidden" name="reqtype" value="edit">
                                                              <input type="hidden" name="get_user_id" value="<?php echo $user_id; ?>"/>
                                                               <button type="submit" class="btn blue">
                                                                <i class="fa fa-check"></i> Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                       <!-- modal -->
                                                    <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                                                      <div class="modal-dialog">
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Delete</h4>
                                                    </div>
                                                    <div class="modal-body">Are you sure?</div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                        <input type="hidden" name="oldvalue">
                                                     
                                                        <a href="user_profile_insert.php?reqtype=del&oldvalue=<?php echo "$user_id";?>" class="btn btn-danger">Confirm</a>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                                
                            </div>
                        </div>
                        
             
						<!-- END Kaizen page Content kulsum03end-->					
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php include "footer.php"; ?>
            <!-- END FOOTER -->
        </div>
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS kulsum04start-->
		<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS kulsum04end-->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS kulsum05start -->
        <script src="assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS kulsum05end -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>