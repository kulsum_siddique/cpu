   <!-- BEGIN FOOTER -->
   <div class="page-footer">
        <div class="page-footer-inner"> 2018 &copy; Clipping Path Universe.</div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
     </div>
    <!-- END FOOTER -->