<?php
	// Start session
	session_start();
	$_SESSION['USEREMAIL'];
	// Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['USERNAME']) || (trim($_SESSION['USERNAME']) == '')) {
		header("location: access-denied.php");
		exit();
	}
?>