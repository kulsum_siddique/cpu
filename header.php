<?php

// validating if user logged in or not

    require_once("auth.php");
   

	?>

            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.php">
                            <img src="assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
                    <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
                    <div class="hor-menu   hidden-sm hidden-xs">
                        <ul class="nav navbar-nav">
                            <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                            <li class="classic-menu-dropdown active" aria-haspopup="true">
                                <a href="client_dashboard.php"> <i class="fa fa-dashboard"></i> Dashboard
                                    <span class="selected"> </span>
                                </a>
                            </li>
                            
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"><i class="fa fa-suitcase"></i> Orders
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li>
                                        <a href="my_orders.php">
                                            <i class="fa fa-bookmark-o"></i> My Orders </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="place_order.php">
                                            <i class="fa fa-upload"></i> Place an Order </a>
                                    </li>
                                    <li>
                                        <a href="req_quote.php">
                                            <i class="fa fa-puzzle-piece"></i> Request a quote </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="download.php"> <i class="fa fa-download"></i> Download
                                    <span class="selected"> </span>
                                </a>
                            </li>
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="invoice.php"> <i class="fa fa-dollar"></i> Invoice
                                    <span class="selected"> </span>
                                </a>
                            </li>
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="account.php"> <i class="fa fa-user"></i> Account
                                    <span class="selected"> </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                    
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            
                            
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"><?=$_SESSION['USERNAME'];?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    
                                    <li class="divider"> </li>
                                    
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->